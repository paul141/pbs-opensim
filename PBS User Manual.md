Power Boat System

User Manual V1.0

Edison Rex @ hg.zetaworlds.com:80

Contents:

1) Introduction
2) The Scripts
3) Building a Boat
4) Extending and Modifying the PBS System
5) Appendix I - Messages


1) Introduction

Power Boat System came about because I felt the lack of realistic physical boating, especially in ubODE, was a problem that needed to be fixed. There are a number of different script systems (or just some individual scripts) that implement a boat, some a bit more successfully than others. None of the scripts I've run into did everything I wanted, which is to accurately model a power boat, both with the navigational characteristics and the simulation features. Specifically, to have an advanced boat/engine model that crosses sims, consumes resources such as fuel, drifts with wind, and has a well defined system for implementing instrumentation (and has instruments working with it).

This is a complete build from scratch. Some of the code might look familiar, that's probably a coincidence because some things, there's really only one good way to do them. it is designed for ubODE physics. You could run this system on Bullet but it won't work as well. 

The system consists of a main script, which resides in the root prim of your watercraft, some required components for the script (a sit pose, basically) and a number of accessory scripts which live in various child prims linked into your build. Certain parameters are configurable, to make the boat match the physics. 

2) The Scripts

The primary features of the main script are as follows:

a) works on a timer loop, with keyboard controls. there is no "hud" although one would be easily enough implemented.
b) a messaging system for marine subsystems (engine, electrical, mechanical, instruments) which allows for extending the system as well as customising it.
c) a configuration menu
d) accurate physical model using ubODE physics.
e) usability features (20 step throttle, auto-heading hold, docking mode, anchor and mooring mode)

If your simspace uses Ocean Engineering wind (or the equivalent RL reporting from WeatherOS) you can use that wind to introduce drift into your model. The script can also use sim wind, although it is not recommended. With wind enabled, anchor and moor mode turn the boat into the wind. In running mode, the boat will drift.

Basic Operation of the Main Script

<-  ->     (steering is with left and right arrow keys)

^   v       (throttle and directiion are done with the up and down arrow keys)

pageup pagedown  (operation mode select)

shift->      (menu select) -or- mouse mode steering

<-shift     (weather report) -or- mouse mode steering

Sitting on the boat as owner (or as pilot, if guest access is a allowed) starts the engine. Alternatively the engine may be stopped and restarted from the sitting position using the page (up and down) keys (mode change). 

Throttle and transmission (forward and backward) is done via the arrow-up and arrow-down keys. There are 20 detents to the throttle. 

Mode changes are generally achieved via page up and page down. When the boat engine starts (as you sit), the boat's mode automatically changes to "docking" mode. This shuts off drift in the dock, allowing you to get out of the dock. 

From there, a single page-up will enter "running" mode, which turns wind drift effects on.  A further page-up will set the auto-heading hold mode, which will hold the wheel at whatever heading you were on when you selected it. It is released when you steer (right and left arrow keys).

the page down key will select operation modes, in the following order:

- docking mode (disable drift)
- shutdown mode (shutdown engine)
- anchor mode (stop engine, don't disable electric systems, face the wind, don't drift)
- mooring mode (stop engine, disable electric systems, face the wind, don't drift)

further use of down arrow will start the engine in docking mode, and repeat the process.

shift-right and shift-left will steer in mouselook mode, and will otherwise give a weather report or configuration menu outside of mouselook mode. NOTE! mouselook mode cannot be reliably detected after crossing a region, it's highly likely you'll lose steering ability in mouselook mode after a region crossing.

2-1) The Engine Script

The main script implements the motor force parts of the physical engine (boat movement). However, a separate script implements the engine sounds. You can make your own engine sounds and replace the ones that are supplied. There is a separate startup sound.

2-2) Instrument Needle Deflections

Various instruments receive messages and values, and translate those values into a deflection of a meter needle. Speedometer, Tachometer, Battery, Water, Oil and Fuel all accept these sorts of messages

2-3) Smoke, Wake, Propellers

Particle generators are set to emit particles upon movement messages from the main script. Propellers will also rotate as well as emit particles (cavitation effects). 

2-4) Warning Lights, Status Indicators

System trouble indicator messages can be received by some scripts and "light up" warning indicators. The transmission also indicates direction and this can be sensed. See the Furuno FI-70 Engine Display for examples.

2-5) The Menu

Touching the boat as owner without being sat on it will bring up a setup menu. You can also get the menu with shift-right when sat on the boat. 

a) Engine (Simple, Advanced) - use a simple or advanced engine model. Simple doesn't take fuel, oil pressure, water temperature, or electrical effects into account. Advanced does.

b) Wind Type - System or Setter. The setter needs to be either an Ocean Engineering wind setter buoy, or the Weatheros real weather relay buoy. The wind itself is a vector. System wind can be used, consider actually using the wind module instead of the random wind.

c) Access - Private or Public - Private, owner only. Public, anybody can drive the boat. Only the owner can set the menus.

2-6) Transponder

The system utilises a transponder to help locate the boat and owner. When the boat is in operation, the transponder broadcasts to the Bolderbay.net radar site. Additional accessories and products can use the output of the radar site, but you can also just look at it (it's not much to look at raw). When you disembark the boat, the transponder continues to run for 1 hour. This can be useful if you crashed out of your viewer whilst driving the boat, as your boat will keep running since it doesn't know you unsat. The boat's current region, position, direction and speed are sent and displayed. 

http://bolderbay.net/radar/output.php

See Edison Rex @ hg.zetaworlds.com:80 for addition information on WeatherOS, the Bolderbay radar site, or any of the other accessories designed to work with this system.



3) Building a Boat

Your boat's root prim will hold the script. It is recommended that this is either a keel or the below deck or hull. Whichever prim you use as the root prim must be orientated purely <0,0,0> and NOT rotated. This is important. If you are animating a boat whose root prim is not correctly oriented, just build a keel and attach the boat to the keel. The keel should be somewhat near the waterline - it's not absolutely necessary, but you'll have to adjust the height above water otherwise. 

Place the main script and the driver's pose in the root prim. 

In the main script you will need to edit the pilot's position. 

Place the engine script along with the engine sounds either in an engine prim, or belowdecks. This way the engine sounds like it's in the right place.

Fit gauges onto your instrument cluster. Fir the propeller(s) and position the smoke generators near the exhaust port(s). Fit a wake generator at the bow near the waterline. Mind the orientation of the generator, as it will send two streams of particles out. Set all particle generators to opaque

You can add extra sits to the boat using something like the Magic Sit Kit. I've not tried AvSitter, however I am pretty sure PMAC will not work well with this system.

4) Extending and Modifying the PBS System

Although the PBS source code is not public domain, extension and modification is encouraged via the messaging standards. Building your own instruments is a great way to learn scripting techniques, and adding onto the system (for example adding additional boat functionality) is encouraged as well. The main script engine, while not public domain, is intended to not be sold. You are free to sell your own compatible creations as you wish, but do not sell a creation with the pbs script within. Should you wish to license the script to include in a boat for sale, please contact me.

5) Appendix I - Message Format

PBS messages are sent generally by the main script, via link messages to child prims. A link relay could easily be made to transmit to a HUD (and may be something I get around to doing soon). 

Messages consist of both an integer message type and a string message content section. Receivers should check for their integer message type (it's a constant) and convert the string message to the appropriate data cast. 

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started

Start and Stop have no additional text messages. A listening routine would use the start message to normalise the state of the linked child's function (for example having the engine play the start sound), and a stop message should set the linked device to a stopped state (for instance deflecting a needle to zero deflection to simulate power off).

integer pbsMsgThrottle = 21;                // throttle message, string has the value

Throttle messages are signed integers. They should range from -20 to +20. These can be used by, for example, a throttle handle to set the angle. Throttle and Tachometer (RPM) are separate messages, because they do separate things and RPM is not linear.

integer pbsMsgFuel = 22;                    // fuel message, string has the value

Fuel quantity can be set in the main script. Fuel consumption rate is set in the main script as well, it's a factor related to engine RPM. A fuel gauge would need to scale from the size of the tank (the default is 1000l of fuel). The quantity is sent as a positive float value converted to string. 

integer pbsMsgOil = 23;                     // oil pressure, string has the value

Oil pressure is sent as a float value converted to string. The advanced engine will increase oil pressure at higher RPMs.

integer pbsMsgH2oTemp = 24;             // water temp, string has the value

Water temperature is sent as a float value converted to string. The advanced engine will accumulate temperature at higher RPMs and ambient temperatures. Over temperature is possible and will send a WARN message.

integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions

This is an extensible message type. The message component contains a comma separated list of warning conditions in effect at the time. OIL, H2O, BATT, FUEL, ENGINE are examples. 

integer pbsMsgTach = 26;                    // tach, string has the value

RPM is the message component, always a positive integer converted to string. Tach messages are given by the engine function. Engine RPM is a factor of throttle * rpm factor which means you can adjust your engine RPM range (and can include over-RPM conditions if you wish). 

integer pbsMsgVolts = 27;                   // battery, string has the voltage

The message component is a float value converted to string. When the engine is running, this value will increase with RPM.

integer pbsMsgAmps = 28;                    // ammeter, string has the current

This message component is a float value converted to a string. The simulation will take into account current draw, and battery charge. Current draw over time with no charge will result in a dead battery, which will leave you adrift.

integer pbsMsgDirection = 29;               // transmission direction

The message component consists of three values (-1,0,1) indicating the transmission's current direction (reverse, neutral and forward). 

integer pbsMsgWake = 30;                    // messages to wake generators

Because wake shouldn't start until the boat is in motion, separate start and stop messages are sent for a Wake event.

integer pbsMsgSmoke = 31;                   // messages to smoke generators

Smoke messages currently are either on or off. 

integer pbsMsgWindVane = 32;                // messages to windvanes

The improved bwind windvane (ask me for it) which attaches to the Zetaworlds modified bwind script, will accurately point the vane into the wind, even as the boat turns, for any boat direction. Wind is typically received via WeatherOS buoys, or OE setter buoys (or you can write your own, it's fairly trivial), or system wind. In any case, the message sent to the vane in the message component is a float message giving the wind direction in degrees (coming from). The windvane will convert back to radians. This will work for banner flags and telltales too, also you could make an instrument to indicate wind direction.



