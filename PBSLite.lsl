// Power Boat System (Lite version)
// (c) Paul Harriman (dba Edison Rex) 2018
// GPL license. See the Readme and LICENSE files for all the relevant permissions.
// Generally you can use this code freely.
// The API to this system is open, feel free to write your own accessories.
//
// Physical power boat system. What distinguishes this from others is the feature set:
// - simple control set for forward, reverse, turns
// - physical model crosses sim boundaries. ubODE physics
// - accessories can be added
//
// this version is Lite 0.9-3 (development fork)
//
// history
// 0.9 fork from 0.5.2 Main version
// This version strips out advanced features but preserves the physics and basic functions of the power boat system.
// It is designed for general use in ubODE.
// It will still accept most PBS accessories however engine parameters are generally static.
// 0.9-1 change engine accelleration to exponential and use less steps
// 0.9-1 pbsEngineV1.2 adds preload, queueing and engine transition sounds
// 0.9-2 steering proportional to speed, also nose up on turns
// 0.9-3 fixed some bugs in the angular motor too! also won't lose the arrow keys on crossing
//
// THIS SHOULD NOT BE EXTENDED. USE PBS-MAIN.
//
// Modify these for the boat you're putting this in.
//
string pbsLiteVersion = "0.9.3";            // I use this. You don't have to.
string pbsPose="pbsPilotPose";              // animation name (either name your pose this, or change this to the name of your pose)
vector pbsPilotSitPos = <-1.8,0.4,0.6>;     // where to place the pilot on this boat, modify this to suit your pilot's position
rotation pbsPilotRotation = <0.0,0.0,0.0,0.0>; // pilot's rotation in place
float keelHeight = -0.6;                     // keel's actual position in relation to sea level
integer pbsChatty = TRUE;                  // whether or not whispers occur. Set to TRUE to get whispers.

// Global Constants
//
// PBS constants (please do not modify these, you risk incompatibilities with others)
// mode constants (operating mode) 
// 
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeRunning = 1;                     // running, at least a pilot is on board
integer pbsModeStopped = 2;                     // stop engine

// permission constants
integer pbsPermPrivate = 2000;                     // Only owner can pilot 
integer pbsPermGroup = 2001;                      // Group members can pilot
integer pbsPermPublic = 2002;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil pressure, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current
integer pbsMsgDirection = 29;               // transmission direction

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

integer pbsSetEngineAdvanced;
integer pbsSetWindType;
integer pbsPerms;                          // run time permissions
integer pbsAccess;                          // who can drive this boat?
integer pbsLastMenu;                        // I think we save the menu

integer pbsMSelClose = 1000;
integer pbsMSelMain = 1001;
integer pbsMSelEngine = 1002;
integer pbsMSelWind = 1003;
integer pbsMSelPerm = 1004;

integer pbsUseSystemWind = 2020;
integer pbsUseSetterWind = 2021;

// End Constants

// Globals - some of these you can safely modify, read the comments.
key pbsPilotPose;                           // pose key
float pbsCompass;                           // main compass 

// these generally don't need modification for the boat to work

integer permSet;                            // this is how the script checks who 
integer mouseLook = FALSE;                  // are we in mouselook mode

float thrust = 0.25;                         // how much physical force to move with (throttle * thrustMult)
float throttleMax = 10.0;               // how many clicks on the throttle
float throttle;
float thrustMult = 2.0;  // Thrust multiplier, this will get replaced by engine calls sometime
integer dispThrot;               
integer thrustPercent = 0;
float speed;                                    // speed in m/s
float maxSpeed = 23.0;                            // max speed in m/s

float speedRatio;
float speedTurn;
float turnStrength = .30;                       // turn strength, how much to rotate Z axis

integer timerDiv = 0;

// engine params
//

float fuelMax=1000;                     // full tank
float fuelFactor;                       // 
float fuelRemain;
float fuelFlow;                         // fuelFlow = throttle*fuelFactor
float oilPress;
float oilBase;
float oilFactor;                        // oilPress = oilBase+(throttle*oilFactor)
float h2oTemp;
float h2oBase;
float h2oFactor;                        // h2oTemp = h2oBase+(throttle*h2oFactor)
string warnList;                        // for warning lights, comma separated list
float tachRPM;
float tachFactor;
float tachIdle;                         // tachRPM = abs(throttle*tachFactor) (tachIdle is for idle only)                    
integer runTimer;                       // engine run time
float battBase;                         // battery base voltage (affected by current drain)
float battVolts;
float battFactor;                       // battVolts = battBase+abs(throttle*battFactor)
float chargeAmps;                       // ammeter charge or discharge
integer direction;                      // transmission's direction, 3 states.

vector angular_motor;                   // motor params
float boatPitch;                        // pitch nose up for higher speeds
vector linear_motor;
vector global_vel;                          // velocity trackers 
vector local_vel;

float seaLevel = 20.0;                      // where we decided sealevel is
vector wind;                                // received wind
float windSpeed;                            // magnitude of wind
float windDir;                            // the Z component of wind converted to degrees
float drift;

vector eulerRot;                            // our Euler rotation
vector currEuler;                           // our current Euler rotation (compared with above)
rotation quatRot;                           // rotations, part of llGetRot() stuff
rotation currRot;
float zRotAngle;                            // z rotation angle

float compass;                              // compass heading (not necessarily boat heading)

float timerInt = 1.0;                           // timer interval 

integer OnBoard = FALSE;                    // Pilot onboard?
integer pbsMode;                            // current PBS operating mode


key pilot = NULL_KEY;                       // who's trying to sit on the boat
key owner = NULL_KEY;                   // who owns the boat
key group = NULL_KEY;                   // group the boat is in

// Camera Variables

integer camEnabled = FALSE;
list drive_cam =
[
        CAMERA_ACTIVE, TRUE,
        CAMERA_BEHINDNESS_ANGLE, 6.3,
        CAMERA_BEHINDNESS_LAG, 0.6,
        CAMERA_DISTANCE, 21.0,//3.0,
        CAMERA_PITCH, 15.0,
// CAMERA_FOCUS,
        CAMERA_FOCUS_LAG, 0.05,
        CAMERA_FOCUS_LOCKED, FALSE,
        CAMERA_FOCUS_THRESHOLD, 0.0,
// CAMERA_POSITION,
        CAMERA_POSITION_LAG, 0.3,
        CAMERA_POSITION_LOCKED, FALSE,
        CAMERA_POSITION_THRESHOLD, 0.0,
//
        CAMERA_FOCUS_OFFSET,<0,0,1>//<0,0,1>
];

// Functions (purposefully making this script simple)

// load boat settings
pbsLoadDefaultState()
{
        //pbsPilotSitPos = <-1.25,-1.0,3.65>;                             // there was no card. Use these defaults.
        pbsPilotRotation = <0.0,0.0,0.0,0.0>;
        pbsAccess = pbsPermPublic;
        pbsSetEngineAdvanced = FALSE;
        //thrustMult = 1.25;
        //maxSpeed = 11.3;

        fuelRemain = 1000;
        fuelFactor = .01;
        oilBase = 50.0;
        oilFactor = 1.0;                         // oil pressure rise (per throttle tick)
        h2oBase = 95.0;
        h2oFactor = .4;                         // water temp rise (degrees per throttle tick)
        tachFactor = 240.0;                     // each throttle tick increases by this amount
        tachIdle = 720.0;                       // tach at idle
        battBase = 12.5;                        // battery base voltage
        battFactor = 0.15;                       // RPM factor        
 
}

  
// init the boat. Mostly set up the vehicle parameters

pbsInit()
{
    if (pbsChatty) llWhisper(0,"PBS Lite "+pbsLiteVersion);
    // disable camera for now. Needs a rethink
    llSetCameraEyeOffset( < -20, 0., 8.5 > ); // Position of camera, relative to parent.
    llSetCameraAtOffset( <0, 0, 5.0 > );        // Point to look at, relative to parent.

    llSetVehicleType( VEHICLE_TYPE_BOAT );
    llSetVehicleRotationParam(VEHICLE_REFERENCE_FRAME,ZERO_ROTATION); 
    
    llSetVehicleFlags(VEHICLE_FLAG_NO_DEFLECTION_UP|
                                VEHICLE_FLAG_HOVER_GLOBAL_HEIGHT|
                                VEHICLE_FLAG_LIMIT_MOTOR_UP ); 
                                        
    //linear motion
    
    llSetVehicleVectorParam  (VEHICLE_LINEAR_FRICTION_TIMESCALE,<50.0,2.0,0.5>);;
    llSetVehicleVectorParam  (VEHICLE_LINEAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_TIMESCALE,10.0);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_DECAY_TIMESCALE,60);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_EFFICIENCY,0.85);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_TIMESCALE,1.0); 
    
    //angular motion
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_FRICTION_TIMESCALE,<5,0.1,0.1>);
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_TIMESCALE,0.1);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_DECAY_TIMESCALE,.2);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_EFFICIENCY,0.7);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_TIMESCALE,0.7);
                                                //default 1.0 -- reduce to have more lateral drift (like 0.3 - 0.5)
                                                
    //vertical attractor
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_TIMESCALE,3.0);
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_EFFICIENCY,0.8);
    
    //banking
    llSetVehicleFloatParam   (VEHICLE_BANKING_EFFICIENCY,0.6);
    llSetVehicleFloatParam   (VEHICLE_BANKING_MIX,1.0);
    llSetVehicleFloatParam   (VEHICLE_BANKING_TIMESCALE,0.8);
    
    //vertical control
    llSetVehicleFloatParam   (VEHICLE_HOVER_HEIGHT,(seaLevel-keelHeight));
    llSetVehicleFloatParam   (VEHICLE_HOVER_EFFICIENCY,2.0);
    llSetVehicleFloatParam   (VEHICLE_HOVER_TIMESCALE,1.0);
    llSetVehicleFloatParam   (VEHICLE_BUOYANCY,1.0);
    
    // engine factors and points (for Lite, just poke them)
    pbsLoadDefaultState();      // use standard defaults
    
    // finish initialising things
    direction = 0;                          // transmission neutral
    
}

// Reset the powerboat to initial settings

pbsReset()
{
    if (pbsChatty) llWhisper(0,"Resetting...");
    llSetSitText("Pilot");
    owner = llGetOwner();
    llSetStatus(STATUS_ROTATE_X | STATUS_ROTATE_Z | STATUS_ROTATE_Y,TRUE);
    llSetStatus(STATUS_PHYSICS,FALSE);
    llSetStatus(STATUS_PHANTOM,FALSE);
    llSetStatus(STATUS_BLOCK_GRAB,TRUE);
    llSetTimerEvent(0);
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStop, "stop", NULL_KEY);
    llSitTarget(pbsPilotSitPos,pbsPilotRotation);
    pbsPilotPose = NULL_KEY;
}

pbsEngineStart()
{
    llSay(0,"Engine Start");
    llSetStatus(STATUS_ROTATE_X | STATUS_ROTATE_Z | STATUS_ROTATE_Y,TRUE);
    llSetStatus(STATUS_PHYSICS,TRUE);
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStart, "start", NULL_KEY);    // startup boat instruments etc
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "11", NULL_KEY);       // battery drain
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "-70", NULL_KEY);       // current to start engine
    throttle = 0;
    direction = 0;
    runTimer = 0;
    // play engine start here
    // sleep to allow it to play
    // llSleep(1.0); 
    llSetTimerEvent(timerInt);
    // send messages to fuel, oil and water gauges, etc
    // also set tachometer to idle
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "14.7", NULL_KEY);       // battery charge
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "30", NULL_KEY);       // current to charge battery
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgThrottle, (string)throttle, NULL_KEY);       // throttle
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgDirection, (string)direction, NULL_KEY);       // transmission
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgSmoke, "ON", NULL_KEY);       // exhaust

    pbsMode = pbsModeRunning;
    
    if ( pbsPerms & PERMISSION_CONTROL_CAMERA )
    {
        if (pbsChatty) llWhisper(0,"cam enabled");
        llSetCameraParams(drive_cam);
        camEnabled = TRUE;
    }
}

pbsEngineStop()
{
    if (pbsChatty) llSay(0,"Engine Shutdown");
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStop, "stop", NULL_KEY);    // shutdown boat instruments etc
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "8.0", NULL_KEY);       // battery drain
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "0", NULL_KEY);       // current to start engine 
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgSmoke, "OFF", NULL_KEY);       // exhaust
    if ( pbsPerms & PERMISSION_CONTROL_CAMERA )
    {
        
        llClearCameraParams();
        camEnabled = FALSE;
    }


}   

pbsEngine()
{

    if (pbsMode == pbsModeRunning)
    {
        // direction is for the transmission indicator
        // otherwise we use absolute value for the throttle
        
        runTimer = runTimer + 1;                // tick tock
        
        if (throttle < 0) direction = -1;                     // three states. Guess which ones
        else if (throttle > 0) direction = 1;
        else direction = 0;

        thrust = throttle * (thrustMult * thrustMult);                 // thrustmult is your engine power multiplier
        thrustPercent = (integer)((throttle / throttleMax) * 100.0);  // percent has to be scaled for detents
        
        //
        // motor thrust in Running mode
        if (pbsMode == pbsModeRunning)
        {
            linear_motor.x = thrust + (-angular_motor.x/2.5);
            linear_motor.y = 0.0;
            // linear_motor.z = (thrust/5.0);         // lift out of the water at higher speeds
            // llSetVehicleFloatParam   (VEHICLE_HOVER_HEIGHT,(seaLevel-keelHeight)+(throttle/50));
            //boatPitch = -(thrust/50);
            //angular_motor.y = boatPitch;            // pitch up at higher speeds
        }
        else
        {
            linear_motor.x = thrust;
            linear_motor.y = 0.0;
        }
        
        linear_motor.z = 0.0;       
        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, linear_motor);
        // llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, angular_motor);

        speed = llVecMag(llGetVel());
        speedRatio = speed/maxSpeed;
        speedTurn=1.2*(speedRatio/1.5);
        
        if (throttle == 0)
        {
            tachRPM = tachIdle;
        }
        else
        {
            tachRPM = llFabs(throttle*tachFactor);
        }
        
            fuelFlow = 0;
            oilPress = 80.0;
            h2oTemp = 100.0;
            chargeAmps = 5.0;
            battVolts = 14.2;
        
        pbsInstruments();                       // send out the instrument messages
            
        if (llFabs(throttle)>0)
        {
            llMessageLinked(LINK_ALL_CHILDREN, pbsMsgWake, "wakeon", NULL_KEY);
        }
        else
        {
            llMessageLinked(LINK_ALL_CHILDREN, pbsMsgWake, "wakeoff", NULL_KEY);
        }


    }
    else
    {
        if (pbsMode == pbsModeStopped)
        {
            llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
        }           
    }
            
}

    
pbsInstruments()
{
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgFuel, (string)fuelRemain, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgThrottle, (string)throttle, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgTach, (string)tachRPM, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgDirection, (string)direction, NULL_KEY);  // transmission
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgOil, (string)oilPress, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgH2oTemp, (string)h2oTemp, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgVolts, (string)battVolts, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgAmps, (string)chargeAmps, NULL_KEY);

}

// main line code here

default
{
    state_entry()
    {
        pbsReset();
        pbsInit();
    }
    
    on_rez(integer param)
    {
        llResetScript();
    }
        
    changed(integer change)
    {
        pilot = llAvatarOnSitTarget();

        
        if (change & CHANGED_LINK)
        {
            
            if (pilot==NULL_KEY)
            {
                OnBoard = FALSE;
                
                if (!(llGetAgentInfo(pilot) & AGENT_ON_OBJECT)) 
                {  
                    pbsReset();
                }
                
                if (permSet)
                {
                    llReleaseControls();
                    permSet = FALSE;
                    //llResetScript();
                }                                               // if (permSet)
            }
            else
            {
                if (llGetAgentInfo(pilot) & AGENT_ON_OBJECT)
                {
                    OnBoard = FALSE;                                    // seated but not onboard
                                                                        // can we use this boat?
                                                                        // for each mode see if we're allowed.
                    if (pbsAccess == pbsPermPrivate)                    // if owner access, check owner
                    {
                        if (pilot == owner)
                        {
                            OnBoard = TRUE;
                        }
                    }


                    if (pbsAccess == pbsPermGroup)                      // if group access, check group
                    {
                                
                                key groupKey = llList2Key(llGetObjectDetails(llGetLinkKey(LINK_ROOT), [OBJECT_GROUP]), 0);
                                if (llSameGroup(groupKey))
                                {
                                    if (pbsChatty) llWhisper(0,"Group Access");
                                    OnBoard = TRUE;
                                }
                    }
                          
                    if (pbsAccess == pbsPermPublic)                     // if public access, well, just let em sit
                    {
                                    if (pbsChatty) llWhisper(0,"Public Access");
                                    OnBoard = TRUE;
                    }
                        
                    if (OnBoard)
                    {    
                        llWhisper(0,"you can use this boat");
                     
                        llRequestPermissions(pilot,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
                        //pbsInit();
                        pbsPilotPose = pilot;
                        llStopAnimation("sit");
                        llStartAnimation(pbsPose);
                        pbsEngineStart();
                    }
                    else
                    {
                        llWhisper(0,"sorry, you cannot use this boat.");
                        llUnSit(pilot);
                    }                                           // if (OnBoard)
                    
                }                                               // if (llGetAgentInfo(pilot) & AGENT_ON_OBJECT)
                
            }                                                   // if (pilot==NULL_KEY)
        }                                                       // if (change & CHANGED_LINK)
    }               // end changed event
    
    run_time_permissions(integer pbsPerms)
    {
        if (pbsPerms & PERMISSION_TAKE_CONTROLS)
        {
            llTakeControls(CONTROL_RIGHT |
                                  CONTROL_LEFT  |
                                  CONTROL_ROT_RIGHT |
                                  CONTROL_ROT_LEFT |
                                  CONTROL_FWD |
                                  CONTROL_BACK |
                                  CONTROL_DOWN |
                                  CONTROL_UP,
                                  TRUE, FALSE);
            permSet = TRUE;
        }
    }
    
    control(key id, integer held, integer change)
    {
        
            if ((change & held & CONTROL_ROT_LEFT) || (held & CONTROL_ROT_LEFT)) // left turn
            {
                llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <-speedTurn,(boatPitch/3),turnStrength>);
            }

        
            if ((change & held & CONTROL_ROT_RIGHT) || (held & CONTROL_ROT_RIGHT)) // right turn
            {
                llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <speedTurn,(boatPitch/3),-turnStrength>);
            }
                    
            if  (change & held & CONTROL_FWD) // throttle up 
            {
                if (throttle < throttleMax)
                {
                    throttle = throttle + 1;
                    dispThrot = (integer)throttle;
                    if (pbsChatty) llWhisper(0,"throttle "+(string)dispThrot);
                }
            }
        
            if  (change & held & CONTROL_BACK) // throttle down 
            {
                if (throttle >  -throttleMax)
                {
                    throttle = throttle - 1;
                    dispThrot = (integer)throttle;
                    if (pbsChatty) llWhisper(0,"throttle "+(string)dispThrot);
                }
            }
                                
            if ((change & held & CONTROL_RIGHT)) // if in mouselook, turn right as normal)
            {
                
                if (mouseLook & AGENT_MOUSELOOK)
                {
                    llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <speedTurn,(boatPitch/3),-turnStrength>);
                }
            }
            
            if ((change & held & CONTROL_LEFT)) // if in mouselook, turn right as normal)
            {
                
                if (mouseLook & AGENT_MOUSELOOK)
                {
 
                    llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <-speedTurn,(boatPitch/3),turnStrength>);
                }
            }      
    }

// Timer event, this is where the action happens.
    
    timer()
    {
        currEuler = llRot2Euler(llGetRot());            // get heading
        zRotAngle = currEuler.z;
        pbsCompass = zRotAngle*RAD_TO_DEG;              // that's the compass, we need it for heading hold and drift
        pbsCompass = (450.0 - pbsCompass)%360.0;
        mouseLook = llGetAgentInfo(pilot);              // maybe tell is if we're in mouselook
        pbsEngine();                                    // make the boat move
    }
              
}       