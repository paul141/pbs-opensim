// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// Licensed GPL. See the LICENSE file.
// The API to this system is open, feel free to write your own accessories.
//
// Accessory to the PBS. This implements a spinning propellor.
// It will emit some particles and spin proportionally to the throttle and direction.
// It will receive messages from the PBS main script. 

// this version is 1.0 
//

// Global Constants (PBS-Constants.h)
//
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeMoored = 1;                      // moored, nobody onboard, drifts with wind
integer pbsModeAnchored = 2;                    // anchored, someone onboard, drifts with wind
integer pbsModeRunning = 3;                     // running, at least a pilot is on board
integer pbsModeAdrift = 4;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 5;                     // Only owner can pilot 
integer pbsPermGroup = 6;                      // Group members can pilot
integer pbsPermPublic = 7;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current
integer pbsMsgDirection = 29;               // transmission's direction for propellor spin

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

// variables for this script

float spinSpeed;                            // turn the prop how fast
float spinFactor = 2.0;                     // spin factor
integer direction = 1;

emitOn()
{
    llParticleSystem([                   
    PSYS_PART_FLAGS , 0 
    | PSYS_PART_INTERP_COLOR_MASK   //Colors fade from start to end
    | PSYS_PART_INTERP_SCALE_MASK   //Scale fades from beginning to end
    | PSYS_PART_FOLLOW_VELOCITY_MASK//Particles are created at the velocity of the emitter
    | PSYS_PART_EMISSIVE_MASK  // full bright
    ,PSYS_SRC_PATTERN,           PSYS_SRC_PATTERN_ANGLE
    ,PSYS_SRC_TEXTURE,           "ca4102f6-7983-4119-9043-749817ad6ede"           //UUID of the desired particle texture, or inventory name
    ,PSYS_SRC_MAX_AGE,           0.0            //Time, in seconds, for particles to be emitted. 0 = forever
    ,PSYS_PART_MAX_AGE,          2.0            //Lifetime, in seconds, that a particle lasts. Original Value = 8
    ,PSYS_SRC_BURST_RATE,        0.0            //How long, in seconds, between each emission
    ,PSYS_SRC_BURST_PART_COUNT,  1              //Number of particles per emission
    ,PSYS_SRC_BURST_RADIUS,      0.0           //Radius of emission
    ,PSYS_SRC_BURST_SPEED_MIN,   0.5             //Minimum speed of an emitted particle
    ,PSYS_SRC_BURST_SPEED_MAX,   0.5            //Maximum speed of an emitted particle
    ,PSYS_SRC_ACCEL,             <0,0,-0.3>    //Acceleration of particles each second
    ,PSYS_PART_START_COLOR,      <1.0,1.0,1.0>  //Starting RGB color
    ,PSYS_PART_END_COLOR,        <1.0,1.0,1.0>  //Ending RGB color, if INTERP_COLOR_MASK is on 
    ,PSYS_PART_START_ALPHA,      0.75            //Starting transparency, 1 is opaque, 0 is transparent.
    ,PSYS_PART_END_ALPHA,        0.0            //Ending transparency
    ,PSYS_PART_START_SCALE,      <1.0,1.0,0.0>  //Starting particle size
    ,PSYS_PART_END_SCALE,        <2.5,2.5,0.0>  //Ending particle size, if INTERP_SCALE_MASK is on
    ,PSYS_SRC_ANGLE_BEGIN,       45.0* DEG_TO_RAD //Inner angle for ANGLE patterns
    ,PSYS_SRC_ANGLE_END,         120.0* DEG_TO_RAD//Outer angle for ANGLE patterns
    ,PSYS_SRC_OMEGA,             <0.0,0.0,0.0>  //Rotation of ANGLE patterns, similar to llTargetOmega()
            ]);
}

emitOff()
{
    llParticleSystem([]);
}
  
default
{
    state_entry()
    {
        llWhisper(0, "Propeller Present");
        emitOff();
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if (num == pbsMsgThrottle)
        {
            spinSpeed = (float)msg;
            spinSpeed = spinSpeed * spinFactor * direction;
            llTargetOmega(<1.0,0.0,0.0>,spinSpeed,PI);
            if (spinSpeed > 0.0)
            {
                emitOn();
            }
            else
            {
                if (spinSpeed < 0.0)
                {
                    emitOn();
                }
                else
                {
                    emitOff();
                }
            }
        }
        if (num == pbsMsgDirection)
        {
            direction = (integer)msg;
        }
    }                
            
}