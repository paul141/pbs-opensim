Power Boat System

Oil Pressure Gauge (Generic)

This gauge displays the oil pressure as reported by the PBS engine simulation. Oil pressure can change, there is some relationship with water temperature. Too high or too low an oil pressure can disable the engine.

To use it, link it onto your power boat. 

Feel free to modify the gauge texture. The script is included as a notecard as well, so you can see how it works if you're so inclined.

Questions, comments, PM to Edison Rex inworld at hg.zetaworlds.com:80

November 2018
