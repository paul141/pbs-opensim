// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// Licensed GPL, see the LICENSE file
// The API to this system is open, feel free to write your own accessories.
//
// Accessory to the PBS. This implements exhaust smoke.
// It will emit some particles.
// It will receive messages from the PBS main script.

// this version is 1.1
// More particles with more throttle
//
// Global Constants (PBS-Constants.h)
//
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeMoored = 1;                      // moored, nobody onboard, drifts with wind
integer pbsModeAnchored = 2;                    // anchored, someone onboard, drifts with wind
integer pbsModeRunning = 3;                     // running, at least a pilot is on board
integer pbsModeAdrift = 4;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 5;                     // Only owner can pilot 
integer pbsPermGroup = 6;                      // Group members can pilot
integer pbsPermPublic = 7;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current
integer pbsMsgDirection = 29;               // transmission's direction for propellor spin

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

// variables for this script

float burstRate;
float speedMin;
float speedMax;

// functions

emitOn()
{
       llParticleSystem([                   
                                PSYS_PART_FLAGS , 0 
                                | PSYS_PART_INTERP_COLOR_MASK   
                                | PSYS_PART_INTERP_SCALE_MASK   
                                | PSYS_PART_EMISSIVE_MASK   
                                | PSYS_PART_FOLLOW_VELOCITY_MASK
                                | PSYS_PART_WIND_MASK            
                                | PSYS_PART_BOUNCE_MASK
                                ,PSYS_SRC_PATTERN,  PSYS_SRC_PATTERN_ANGLE_CONE
                                ,PSYS_SRC_TEXTURE, "ca4102f6-7983-4119-9043-749817ad6ede"
                                ,PSYS_PART_START_SCALE, <0.2, .2, 0>
                                ,PSYS_PART_END_SCALE, <4,4, 0>
                                ,PSYS_PART_START_COLOR, <.5,.5,.5>
                                ,PSYS_PART_END_COLOR, <.1,.1,.1>
                                ,PSYS_PART_START_ALPHA, 0.8
                                ,PSYS_PART_END_ALPHA, 0.0
                                ,PSYS_SRC_BURST_PART_COUNT, 1
                                ,PSYS_SRC_BURST_RATE, burstRate 
                                ,PSYS_PART_MAX_AGE, 3.0 
                                ,PSYS_SRC_MAX_AGE, 0.0
                                ,PSYS_SRC_BURST_SPEED_MIN, speedMin
                                ,PSYS_SRC_BURST_SPEED_MAX, speedMax
                                ,PSYS_SRC_ANGLE_BEGIN,  0.05*PI
                                ,PSYS_SRC_ANGLE_END, 0.0*PI
                                ,PSYS_SRC_ACCEL, <0.0,0.0,.2>
                                ]);
}

emitOff()
{
    llParticleSystem([]);
}

default
{
    state_entry()
    {
        llWhisper(0, "Exhaust Present");
        emitOff();
    }
    
    link_message(integer sender, integer num, string msg, key id)
    {
        if (num == pbsMsgSmoke)
        {

            if (msg == "ON")
            {
                emitOn();
            }
            else
            {
                if (msg == "OFF")
                {
                    emitOff();
                }
            }
        }
        
        if (num == pbsMsgStop)
        {
            emitOff();
        }
        
        if (num == pbsMsgThrottle)
        {
            burstRate = 0.05 + ((float)msg/50.0);
            speedMin = 0.01 + ((float)msg/100);
            speedMax = 0.1 + ((float)msg/10);
        }
       
    }                
            
}