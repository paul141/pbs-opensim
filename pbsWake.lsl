// ok so it turns particles on and off. If you read this it was based from bwind, but OMG you can do this one statement development in
// Firestorm's editor and inject it. The few lines of code I had otherwise to do are a great example of how to make a PBS accessory, so this script is open.
//
// So I did change a lot here - leaving comments that adequately describe things and adding comments that make more sense.
// also this being PBS, the handler to turn on wake is slightly different from how bwind does it, also FFS stop sending the wake through the boat's hull
// and then letting it sink into the water. Seriously.
//
// this code is totally open and free, gnu license, blah blah, use it how you want it. learn about PBS with it.
//
// 1.1 velocity proportional to speed
float pVelocity = 0.5;

wakeOn()                

{   
    llParticleSystem([                   
    PSYS_PART_FLAGS , 0 
    | PSYS_PART_INTERP_COLOR_MASK   //Colors fade from start to end
    | PSYS_PART_INTERP_SCALE_MASK   //Scale fades from beginning to end
    | PSYS_PART_FOLLOW_VELOCITY_MASK//Particles are created at the velocity of the emitter
    | PSYS_PART_BOUNCE_MASK         // don't sink and kind of bounce at water level
    ,PSYS_SRC_PATTERN,           PSYS_SRC_PATTERN_ANGLE
    ,PSYS_SRC_TEXTURE,           "ca4102f6-7983-4119-9043-749817ad6ede"           //UUID of the desired particle texture, or inventory name
    ,PSYS_SRC_MAX_AGE,           0.0            //Time, in seconds, for particles to be emitted. 0 = forever
    ,PSYS_PART_MAX_AGE,          6.0            //Lifetime, in seconds, that a particle lasts. 6 seconds seems to get behind the boat a bit
    ,PSYS_SRC_BURST_RATE,        0.0            //How long, in seconds, between each emission. 0.0 means continuous
    ,PSYS_SRC_BURST_PART_COUNT,  2              //Number of particles per emission, don't overload the sim with your damn particles
    ,PSYS_SRC_BURST_RADIUS,      0.0           //Radius of emission
    ,PSYS_SRC_BURST_SPEED_MIN,   pVelocity             //Minimum speed of an emitted particle (not fast)
    ,PSYS_SRC_BURST_SPEED_MAX,   pVelocity            //Maximum speed of an emitted particle
    ,PSYS_SRC_ACCEL,             <0,0,-0.1>    //Acceleration of particles each second (the should not sink much, it's suppose to be foam!)
    ,PSYS_PART_START_COLOR,      <1.0,1.0,1.0>  //Starting RGB color (white)
    ,PSYS_PART_END_COLOR,        <1.0,1.0,1.0>  //Ending RGB color, if INTERP_COLOR_MASK is on (white)
    ,PSYS_PART_START_ALPHA,      0.5            //Starting transparency, 1 is opaque, 0 is transparent. (white)
    ,PSYS_PART_END_ALPHA,        0.0            //Ending transparency
    ,PSYS_PART_START_SCALE,      <1.0,1.0,0.0>  //Starting particle size
    ,PSYS_PART_END_SCALE,        <2.5,2.5,0.0>  //Ending particle size, if INTERP_SCALE_MASK is on (it is)
    ,PSYS_SRC_ANGLE_BEGIN,       100.0* DEG_TO_RAD //Inner angle for ANGLE patterns (modified to make a proper bow wake)
    ,PSYS_SRC_ANGLE_END,         130.0* DEG_TO_RAD//Outer angle for ANGLE patterns
    ,PSYS_SRC_OMEGA,             <0.0,0.0,0.0>  //Rotation of ANGLE patterns, similar to llTargetOmega()
            ]);
}

wakeOff()
{
    llParticleSystem([]);
}

default
{
    state_entry()
    {
            wakeOff();          // don't puke out wake particles
    }
    link_message(integer sender_num, integer num, string str, key id)
    {
        if (num==30)            // if you got a pbsMsgWake message
        {
            if(str=="wakeoff")  // string has the command
            {
                wakeOff();
            }
            if(str=="wakeon")
            {
                wakeOn();
            }
        }
        
        if (num==10)            // a pbsmsgStop message means stop
        {
            wakeOff();
        }
        
        if (num == 21)
        {
            pVelocity = .5 + ((float)str / 10.0);
        }
        
    }
}