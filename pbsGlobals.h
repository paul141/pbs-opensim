// Global Constants
//
// PBS constants
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeMoored = 1;                      // moored, nobody onboard, drifts with wind
integer pbsModeAnchored = 2;                    // anchored, someone onboard, drifts with wind
integer pbsModeRunning = 3;                     // running, at least a pilot is on board
integer pbsModeAdrift = 4;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 5;                     // Only owner can pilot 
integer pbsPermGroup = 6;                      // Group members can pilot
integer pbsPermPublic = 7;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes
