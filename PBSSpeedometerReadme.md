Power Boat System

Speedometer (Generic)

This gauge displays the speed of your boat. Technically, the speed is derived independently, but PBS is used to deflect the needle to 0 when the engine is shut down. 

To use it, link it onto your power boat. 

Feel free to modify the gauge texture. The script is included as a notecard as well, so you can see how it works if you're so inclined.

Questions, comments, PM to Edison Rex inworld at hg.zetaworlds.com:80

November 2018
