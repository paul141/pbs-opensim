// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// This is a GPL work, consider this licensed via GPL. If you want to fork it, go ahead.
// The API to this system is open, feel free to write your own accessories.
//
// Physical power boat system. What distinguishes this from others is the feature set:
// - simple control set for forward, reverse, turns
// - separate docking, anchor and moor modes
// - anchor and moor follow wind
// - listens to OE/Weatheros wind, adds drift to powerboating
// - engine dynamics, fuel use, oil temp, water temp, configurable power
// - physical model crosses sim boundaries. ubODE physics
// - accessories can be added
//
// this version is 0.3 (development version)
// history
// 0.1 initial version to get the engine working and the basic controls. No perms working yet,
// but the thing floats and goes.
// 0.2 Engine sounds, wind drift, heading hold, docking mooring and anchoring done.
// 0.3 Menu for settings, setup permissioning system for pilot. Also advanced engine behaviour and instruments.
// 0.4 Bug fix - ignore any sits after the first one, also don't init on unsit
//      implement the configuration note card, to save settings. Use that on init
//      also if OSSL fails, set some rational defaults so the boat is usable.
// 0.4.1 tweak steering. 
//
// Global Constants
//
// PBS constants
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeRunning = 1;                     // running, at least a pilot is on board
integer pbsModeStopped = 2;                     // stop engine
integer pbsModeAnchored = 3;                    // anchored, someone onboard, drifts with wind
integer pbsModeMoored = 4;                      // moored, nobody onboard, drifts with wind
integer pbsModeAdrift = 5;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 2000;                     // Only owner can pilot 
integer pbsPermGroup = 2001;                      // Group members can pilot
integer pbsPermPublic = 2002;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil pressure, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current
integer pbsMsgDirection = 29;               // transmission direction

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

integer pbsSetEngineAdvanced;
integer pbsSetWindType;
integer pbsPerms;                          // run time permissions
integer pbsAccess;                          // who can drive this boat?
integer pbsLastMenu;                        // I think we save the menu

integer pbsMSelClose = 1000;
integer pbsMSelMain = 1001;
integer pbsMSelEngine = 1002;
integer pbsMSelWind = 1003;
integer pbsMSelPerm = 1004;

integer pbsUseSystemWind = 2020;
integer pbsUseSetterWind = 2021;


key pbsMenuKey;
key pbsPilotPose;                           // pose key
string pbsPose="pbsPilotPose";              // animation name
vector pbsPilotSitPos = <-3.95,-1.0,5.35>;     // where to place the pilot on this boat
rotation pbsPilotRotation = <0.0,0.0,0.0,0.0>; // pilot's rotation in place
integer pbsHold;                            // course hold flag
float pbsCompass;                           // main compass
float pbsCourseHoldValue;                    // for heading hold
integer pbsMenuChan=-42001;                 // menu channel

// Basic Global Variables
//

integer permSet;
integer mouseLook = FALSE;                  // are we in mouselook mode

float thrust = 0.25;                         // how much physical force to move with (throttle * thrustMult)
float throttleMax = 20.0;               // how many clicks on the throttle
float throttle;
float thrustMult = 1.25;                     // Thrust multiplier, this will get replaced by engine calls sometime
integer thrustPercent = 0;
float speed;                                    // speed in m/s
float maxSpeed = 11.3;                            // max speed in m/s

float speedRatio;
float speedTurn;

integer pbsUseTransponder = FALSE;
integer timerDiv = 0;

// engine params
//

float fuelMax=1000;                     // full tank
float fuelFactor;                       // 
float fuelRemain;
float fuelFlow;                         // fuelFlow = throttle*fuelFactor
float oilPress;
float oilBase;
float oilFactor;                        // oilPress = oilBase+(throttle*oilFactor)
float h2oTemp;
float h2oBase;
float h2oFactor;                        // h2oTemp = h2oBase+(throttle*h2oFactor)
string warnList;                        // for warning lights, comma separated list
float tachRPM;
float tachFactor;
float tachIdle;                         // tachRPM = abs(throttle*tachFactor) (tachIdle is for idle only)                    
integer runTimer;                       // engine run time
float battBase;                         // battery base voltage (affected by current drain)
float battVolts;
float battFactor;                       // battVolts = battBase+abs(throttle*battFactor)
float chargeAmps;                       // ammeter charge or discharge
integer direction;                      // transmission's direction, 3 states.

vector angular_motor;                   // motor params
vector linear_motor;
vector global_vel;                          // velocity trackers 
vector local_vel;

float seaLevel = 20.0;                      // where we decided sealevel is
float keelHeight = 0.50;                     // keel's actual position in relation to sea level
vector wind;                                // received wind
float windSpeed;                            // magnitude of wind
float windDir;                            // the Z component of wind converted to degrees
float drift;

vector eulerRot;                            // our Euler rotation
vector currEuler;                           // our current Euler rotation (compared with above)
rotation quatRot;                           // rotations, part of llGetRot() stuff
rotation currRot;
float zRotAngle;                            // z rotation angle

float compass;                              // compass heading (not necessarily boat heading)

float timerInt = 1.0;                           // timer interval 

integer OnBoard = FALSE;                    // Pilot onboard?
integer pbsMode;                            // current PBS operating mode
integer HOLD = FALSE;                       // hold flag

key pilot = NULL_KEY;                       // who's trying to sit on the boat
key owner = NULL_KEY;                   // who owns the boat
key group = NULL_KEY;                   // group the boat is in

integer windh=-1;                       // wind setter handle
integer menuh=-1;                       //menu system

// Camera Variables

integer camEnabled = FALSE;
list drive_cam =
[
        CAMERA_ACTIVE, TRUE,
        CAMERA_BEHINDNESS_ANGLE, 6.3,
        CAMERA_BEHINDNESS_LAG, 0.6,
        CAMERA_DISTANCE, 21.0,//3.0,
        CAMERA_PITCH, 15.0,
// CAMERA_FOCUS,
        CAMERA_FOCUS_LAG, 0.05,
        CAMERA_FOCUS_LOCKED, FALSE,
        CAMERA_FOCUS_THRESHOLD, 0.0,
// CAMERA_POSITION,
        CAMERA_POSITION_LAG, 0.3,
        CAMERA_POSITION_LOCKED, FALSE,
        CAMERA_POSITION_THRESHOLD, 0.0,
//
        CAMERA_FOCUS_OFFSET,<0,0,1>//<0,0,1>
];

// Functions (purposefully making this script simple)

// load boat settings
pbsLoadDefaultState()
{
    integer numNCs = llGetInventoryNumber(INVENTORY_NOTECARD);
    integer ncx;
    integer ncd;
    string inLine;
    integer ncFound=FALSE;
    string ncname = ".PARAMS";
    string ncnamex;
    // first let's see if we actually have a default notecard! We might not...
    
    for (ncx=0; ncx<=numNCs; ncx++)
    {
        ncnamex = llGetInventoryName(INVENTORY_NOTECARD,ncx);
        if (llGetSubString(ncnamex,0,8) == ncname) ncFound=TRUE;
    }
    
    // load defaults 
    if (ncFound)
    {
        for (ncx=0;ncx<osGetNumberOfNotecardLines(ncname)-1; ncx++)
        {
            inLine = osGetNotecardLine(ncname, ncx);
            ncd = llSubStringIndex(inLine,"=");
            if (llGetSubString(inLine,0,ncd-1)=="pbsPilotSitPos") pbsPilotSitPos=(vector)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="pbsPilotRotation") pbsPilotRotation=(rotation)llGetSubString(inLine,ncd+1,-1);   
            if (llGetSubString(inLine,0,ncd-1)=="keelHeight") keelHeight=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="pbsAccess") pbsAccess=(integer)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="pbsSetEngineAdvanced") pbsSetEngineAdvanced=(integer)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="thrustMult") thrustMult=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="maxSpeed") maxSpeed=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="pbsUseTransponder") pbsUseTransponder=(integer)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="pbsSetWindType") pbsSetWindType=(integer)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="fuelRemain") fuelRemain=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="fuelFactor") fuelFactor=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="oilBase") oilBase=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="oilFactor") oilFactor=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="h2oBase") h2oBase=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="h2oFactor") h2oFactor=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="tachFactor") tachFactor=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="tachIdle") tachIdle=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="battBase") battBase=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="battFactor") battFactor=(float)llGetSubString(inLine,ncd+1,-1);
            if (llGetSubString(inLine,0,ncd-1)=="maxSpeed") maxSpeed=(float)llGetSubString(inLine,ncd+1,-1);
        }
    }
    else
    {
        pbsPilotSitPos = <-1.25,-1.0,3.65>;                             // there was no card. Use these defaults.
        pbsPilotRotation = <0.0,0.0,0.0,0.0>;
        // keelHeight = 0.30; was set up top
        pbsAccess = pbsPermPrivate;
        pbsSetEngineAdvanced = TRUE;
        thrustMult = 1.25;
        maxSpeed = 11.3;
        pbsUseTransponder = FALSE;
        pbsSetWindType = pbsUseSystemWind;
        fuelRemain = 1000;
        fuelFactor = .01;
        oilBase = 50.0;
        oilFactor = 1.0;                         // oil pressure rise (per throttle tick)
        h2oBase = 95.0;
        h2oFactor = .4;                         // water temp rise (degrees per throttle tick)
        tachFactor = 240.0;                     // each throttle tick increases by this amount
        tachIdle = 720.0;                       // tach at idle
        battBase = 12.5;                        // battery base voltage
        battFactor = 0.15;                       // RPM factor        
    }
    if (pbsAccess == 0)
    {
        llWhisper(0,"osGetNumberOfNotecardLines call failed. Using defaults.");
        llWhisper(0,"OSSL is probably disabled here. You cannot save settings.");
        pbsPilotSitPos = <-1.25,-1.0,3.65>;                             // there was no card. Use these defaults.
        pbsPilotRotation = <0.0,0.0,0.0,0.0>;
        // keelHeight = 0.10;
        pbsAccess = pbsPermPrivate;
        pbsSetEngineAdvanced = TRUE;
        thrustMult = 1.25;
        maxSpeed = 11.3;
        pbsUseTransponder = FALSE;
        pbsSetWindType = pbsUseSystemWind;
        fuelRemain = 1000;
        fuelFactor = .01;
        oilBase = 50.0;
        oilFactor = 1.0;                         // oil pressure rise (per throttle tick)
        h2oBase = 95.0;
        h2oFactor = .4;                         // water temp rise (degrees per throttle tick)
        tachFactor = 240.0;                     // each throttle tick increases by this amount
        tachIdle = 720.0;                       // tach at idle
        battBase = 12.5;                        // battery base voltage
        battFactor = 0.15;                       // RPM factor  
    } 
        
}

// save the default state notecard

pbsSaveDefaultState()
{
     string outputText;
     llRemoveInventory(".PARAMS");                                     // remove current notecard
     llSleep(1.0);                                                          // wait for it to really be removed
     
     outputText = "pbsPilotSitPos="+(string)pbsPilotSitPos+"\n";
     outputText = outputText + "pbsPilotRotation="+(string)pbsPilotRotation+"\n";
     outputText = outputText + "keelHeight="+(string)keelHeight+"\n";
     outputText = outputText + "pbsAccess="+(string)pbsAccess+"\n";
     outputText = outputText + "pbsSetEngineAdvanced="+(string)pbsSetEngineAdvanced+"\n";
     outputText = outputText + "thrustMult="+(string)thrustMult+"\n";
     outputText = outputText + "maxSpeed="+(string)maxSpeed+"\n";
     outputText = outputText + "pbsUseTransponder="+(string)pbsUseTransponder+"\n";
     outputText = outputText + "pbsSetWindType="+(string)pbsSetWindType+"\n";
     outputText = outputText + "fuelRemain="+(string)fuelRemain+"\n";
     outputText = outputText + "fuelFactor="+(string)fuelFactor+"\n";
     outputText = outputText + "oilBase="+(string)oilBase+"\n";
     outputText = outputText + "oilFactor="+(string)oilFactor+"\n";
     outputText = outputText + "h2oBase="+(string)h2oBase+"\n";
     outputText = outputText + "h2oFactor="+(string)h2oFactor+"\n";
     outputText = outputText + "tachFactor="+(string)tachFactor+"\n";
     outputText = outputText + "tachIdle="+(string)tachIdle+"\n";
     outputText = outputText + "battBase="+(string)battBase+"\n";
     outputText = outputText + "battFactor="+(string)battFactor+"\n";
     osMakeNotecard(".PARAMS",outputText);                            // write out notecard
     llWhisper(1,"saved system settings .PARAMS nc");
}
  
// init the boat. Mostly set up the vehicle parameters

pbsInit()
{
    llWhisper(0,"Init");
    // disable camera for now. Needs a rethink
    //llSetCameraEyeOffset( < -20, 0., 8.5 > ); // Position of camera, relative to parent.
    //llSetCameraAtOffset( <0, 0, 5.0 > );        // Point to look at, relative to parent.

    llSetVehicleType( VEHICLE_TYPE_BOAT );
    llSetVehicleRotationParam(VEHICLE_REFERENCE_FRAME,ZERO_ROTATION); 
    
    llSetVehicleFlags(VEHICLE_FLAG_NO_DEFLECTION_UP|
                                VEHICLE_FLAG_HOVER_GLOBAL_HEIGHT|
                                VEHICLE_FLAG_LIMIT_MOTOR_UP ); 
                                        
    //linear motion
    
    llSetVehicleVectorParam  (VEHICLE_LINEAR_FRICTION_TIMESCALE,<50.0,2.0,0.5>);;
    llSetVehicleVectorParam  (VEHICLE_LINEAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_TIMESCALE,10.0);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_MOTOR_DECAY_TIMESCALE,60);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_EFFICIENCY,0.85);
    llSetVehicleFloatParam   (VEHICLE_LINEAR_DEFLECTION_TIMESCALE,1.0); 
    
    //angular motion
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_FRICTION_TIMESCALE,<5,0.1,0.1>);
    llSetVehicleVectorParam  (VEHICLE_ANGULAR_MOTOR_DIRECTION,ZERO_VECTOR);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_TIMESCALE,0.1);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_MOTOR_DECAY_TIMESCALE,.2);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_EFFICIENCY,0.7);
    llSetVehicleFloatParam   (VEHICLE_ANGULAR_DEFLECTION_TIMESCALE,0.7);
                                                //default 1.0 -- reduce to have more lateral drift (like 0.3 - 0.5)
                                                
    //vertical attractor
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_TIMESCALE,3.0);
    llSetVehicleFloatParam   (VEHICLE_VERTICAL_ATTRACTION_EFFICIENCY,0.8);
    
    //banking
    llSetVehicleFloatParam   (VEHICLE_BANKING_EFFICIENCY,0.5);
    llSetVehicleFloatParam   (VEHICLE_BANKING_MIX,1.0);
    llSetVehicleFloatParam   (VEHICLE_BANKING_TIMESCALE,0.8);
    
    //vertical control
    llSetVehicleFloatParam   (VEHICLE_HOVER_HEIGHT,(seaLevel-keelHeight));
    llSetVehicleFloatParam   (VEHICLE_HOVER_EFFICIENCY,2.0);
    llSetVehicleFloatParam   (VEHICLE_HOVER_TIMESCALE,1.0);
    llSetVehicleFloatParam   (VEHICLE_BUOYANCY,1.0);
    
    // engine factors and points (will come from a notecard)
    pbsLoadDefaultState();      // use the notecard, or standard defaults
    
    // finish initialising things
        
    windh = llListen(-54001,"","","");              // get wind handle
    pbsHold = FALSE;                        // heading hold off
    direction = 0;                          // transmission neutral
    
}

// Reset the powerboat to initial settings

pbsReset()
{
    llWhisper(0,"Resetting...");
    llSetSitText("Pilot");
    owner = llGetOwner();
    pbsMenuKey = owner;
    llSetStatus(STATUS_ROTATE_X | STATUS_ROTATE_Z | STATUS_ROTATE_Y,TRUE);
    llSetStatus(STATUS_PHYSICS,FALSE);
    llSetStatus(STATUS_PHANTOM,FALSE);
    llSetStatus(STATUS_BLOCK_GRAB,TRUE);
    llSetTimerEvent(0);
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStop, "stop", NULL_KEY);
    llSitTarget(pbsPilotSitPos,pbsPilotRotation);
    pbsPilotPose = NULL_KEY;
}

pbsEngineStart()
{
    llSay(0,"Engine Start");
    llSetStatus(STATUS_ROTATE_X | STATUS_ROTATE_Z | STATUS_ROTATE_Y,TRUE);
    llSetStatus(STATUS_PHYSICS,TRUE);
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStart, "start", NULL_KEY);    // startup boat instruments etc
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "11", NULL_KEY);       // battery drain
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "-70", NULL_KEY);       // current to start engine
    throttle = 0;
    direction = 0;
    runTimer = 0;
    // play engine start here
    // sleep to allow it to play
    llSleep(1.0); 
    llSetTimerEvent(timerInt);
    // send messages to fuel, oil and water gauges, etc
    // also set tachometer to idle
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "14.7", NULL_KEY);       // battery charge
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "30", NULL_KEY);       // current to charge battery
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgThrottle, (string)throttle, NULL_KEY);       // throttle
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgDirection, (string)direction, NULL_KEY);       // transmission
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgSmoke, "ON", NULL_KEY);       // exhaust

    pbsMode = pbsModeDocked;
    llWhisper(0,"drift disabled");                      // boat starts docked
    
    if ( pbsPerms & PERMISSION_CONTROL_CAMERA )
    {
        llWhisper(0,"cam enabled");
        llSetCameraParams(drive_cam);
        camEnabled = TRUE;
    }
}

pbsEngineStop()
{
    llSay(0,"Engine Shutdown");
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgStop, "stop", NULL_KEY);    // shutdown boat instruments etc
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgVolts, "8.0", NULL_KEY);       // battery drain
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgAmps, "0", NULL_KEY);       // current to start engine 
    llMessageLinked(LINK_ALL_CHILDREN , pbsMsgSmoke, "OFF", NULL_KEY);       // exhaust
    if ( pbsPerms & PERMISSION_CONTROL_CAMERA )
    {
        
        llClearCameraParams();
        camEnabled = FALSE;
    }


}   

pbsEngine()
{

    if ((pbsMode == pbsModeRunning) || (pbsMode == pbsModeDocked))
    {
        // direction is for the transmission indicator
        // otherwise we use absolute value for the throttle
        
        runTimer = runTimer + 1;                // tick tock
        
        if (throttle < 0)
        {
            direction = -1;                     // three states. Guess which ones
        }
        else
        {
            if (throttle > 0)
            {
                direction = 1;
            }
            else
            {
                direction = 0;
            }
        }

        thrust = throttle * thrustMult;                 // thrustmult is your engine power multiplier
        thrustPercent = (integer)((throttle / throttleMax) * 100.0);  // percent has to be scaled for detents
        
        //
        // motor thrust + drift in Running mode
        if (pbsMode == pbsModeRunning)
        {
            linear_motor.x = thrust + (-angular_motor.x/3.0);
            linear_motor.y = (-angular_motor.y/3.0);
        }
        else
        {
            linear_motor.x = thrust;
            linear_motor.y = 0.0;
        }
        
        linear_motor.z = 0.0;
        
        llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, linear_motor);

        speed = llVecMag(llGetVel());
        
        if (speed < 1)
        {
            speedRatio=1.0/maxSpeed;
        }
        else
        {
            speedRatio = speed/maxSpeed;
        }
        
        speedTurn=0.2+(speedRatio/2.0);
        
        if (throttle == 0)
        {
            tachRPM = tachIdle;
        }
        else
        {
            tachRPM = llFabs(throttle*tachFactor);
        }
        
        // Advanced engine consumption parts
        
        if (pbsSetEngineAdvanced)
        {
            fuelFlow = throttle*fuelFactor;
            if (throttle==0)
            {
                fuelFlow=0.001;
            }
            fuelRemain = fuelRemain - fuelFlow;
            oilPress = oilBase+(throttle*oilFactor);
            h2oTemp = h2oBase+(throttle*h2oFactor);
            battVolts = battBase+(throttle*battFactor);
            if (fuelRemain < 2)
            {
                pbsEngineStop();
                pbsMode = pbsModeAdrift;           // you have run out of fuel...
            } 
            chargeAmps = tachRPM * 0.008;            // make the needle move a bit (needs work here)      
        }
        else
        {
            fuelFlow = 0;
            oilPress = 80.0;
            h2oTemp = 100.0;
            chargeAmps = 5.0;
            battVolts = 14.2;
        }
        
        pbsInstruments();                       // send out the instrument messages
            
        if (llFabs(throttle)>0)
        {
            llMessageLinked(LINK_ALL_CHILDREN, pbsMsgWake, "wakeon", NULL_KEY);
        }
        else
        {
            llMessageLinked(LINK_ALL_CHILDREN, pbsMsgWake, "wakeoff", NULL_KEY);
        }


    }
    else
    {
        if (pbsMode == pbsModeStopped)
        {
            llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
            llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
        }
        else
        {   
            if (pbsMode == pbsModeAnchored)
            {

                angular_motor.x = 0.000001;
                angular_motor.y = 0.000001;
                angular_motor.z = windDir;
                llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
                llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
                llSetRot(llEuler2Rot(angular_motor));
            }
            else
            {
                if (pbsMode == pbsModeMoored)
                {
                    angular_motor.x = 0.000001;
                    angular_motor.y = 0.000001;
                    angular_motor.z = windDir;
                    llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
                    llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <0.0,0.0,0.0>);
                    llSetRot(llEuler2Rot(angular_motor));
                }
            }
        }            
    }
            
}

// use the Bolderbay tracking system to report the boat's position.

pbsTransponder()
{
    vector position = llGetPos();

        llHTTPRequest("http://bolderbay.net/radar/input.php?region=" +
                (string)llGetRegionName() +
                "&&name=" + llGetObjectName() +
                "&&pos=" + (string)((integer)position.x) +
                "," + (string)((integer)position.y) +
                "," + (string)((integer)position.z) +
                "," + llKey2Name(llGetOwner()) +
                "," + (string)((integer)pbsCompass) +
                "," + (string)((integer)speed)
                , [], "");
}
                 

pbsDrift()
{
    // calculate angular influences
    drift =  1*-llSin(windDir)*llSin(0.1)*windSpeed;

    angular_motor.x = wind.x;
    angular_motor.y = wind.y;
    angular_motor.z = wind.z;
    //llSetVehicleVectorParam(VEHICLE_LINEAR_MOTOR_DIRECTION, angular_motor);
}

pbsCourse()
{
    float compass=zRotAngle*RAD_TO_DEG;
    compass = (450.0 - compass)%360.0;
    float compDiff = compass - pbsCourseHoldValue;
    
    if (llFabs(compDiff) > 350.00)
    {
        compass = compass + 360.00;
    }
    
    if (compass > (pbsCourseHoldValue+1))
    {
        // need to turn left
        llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<-speedTurn,0.0,(speedTurn)/25.0>);
    }
    else
    {
        if (compass < (pbsCourseHoldValue-1))
        {
            //llWhisper(0,"right turn");
           llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION,<speedTurn,0.0,-speedTurn/25.0>);
        }
    }

}
    
pbsInstruments()
{
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgFuel, (string)fuelRemain, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgThrottle, (string)throttle, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgTach, (string)tachRPM, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgDirection, (string)direction, NULL_KEY);  // transmission
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgOil, (string)oilPress, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgH2oTemp, (string)h2oTemp, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgVolts, (string)battVolts, NULL_KEY);
    llMessageLinked(LINK_ALL_CHILDREN, pbsMsgAmps, (string)chargeAmps, NULL_KEY);

}

pbsMenuMain()
{
    llDialog(pbsMenuKey, "Options:",["Settings","Perms","Wind","Engine","Save","Close"],pbsMenuChan);
}

pbsMenuPerm()
{
    llDialog(pbsMenuKey,"Perms:",["Owner","Group","Public"],pbsMenuChan);
}

pbsMenuWind()
{
    llDialog(pbsMenuKey,"Wind:",["System","Setter"],pbsMenuChan);
}

pbsMenuEngine()
{
    llDialog(pbsMenuKey,"Engine:",["Easy","Advanced"],pbsMenuChan);
}

pbsDisplaySettings()
{
    string setMessage = "Current settings:\n";
    if (pbsSetEngineAdvanced)
    {
        setMessage = setMessage+"Engine is in Advanced mode\n";
    }
    else
    {
        setMessage = setMessage+"Engine is in Easy mode\n";
    }
    if (pbsSetWindType==pbsUseSetterWind)
    {
        setMessage = setMessage+"Setter wind enabled\n";
    }
    else
    {
        setMessage = setMessage+"System wind enabled\n";
    }
    if (pbsAccess == pbsPermPrivate)
    {
        setMessage = setMessage+"Owner Access\n";
    }
    else
    {
        if (pbsAccess == pbsPermGroup)
        {
            setMessage = setMessage+"Group Access\n";
        }
        else
        {
            if (pbsAccess == pbsPermPublic)
            {
                setMessage = setMessage+"Public Access\n";
            }
        }
    }
    llWhisper(0,setMessage);
}

pbsGetWXR()
{
    llWhisper(0,"Weather Report");
    float windFrom = windDir*RAD_TO_DEG;
    llWhisper(0,"Winds are from "+(string)windFrom+" degrees at "+(string)windSpeed+" knots.");
}

// main line code here

default
{
    state_entry()
    {
        pbsReset();
        pbsInit();
    }
    
    on_rez(integer param)
    {
        llResetScript();
    }
    
    touch(integer num)
    {
        menuh=llListen(pbsMenuChan,"",owner,"");
        pbsMenuMain();
    }
    
    changed(integer change)
    {
        pilot = llAvatarOnSitTarget();

        
        if (change & CHANGED_LINK)
        {
            
            if (pilot==NULL_KEY)
            {
                OnBoard = FALSE;
                
                if (!(llGetAgentInfo(pilot) & AGENT_ON_OBJECT)) 
                {  
                    pbsReset();
                }
                
                if (permSet)
                {
                    llReleaseControls();
                    permSet = FALSE;
                    llResetScript();
                }                                               // if (permSet)
            }
            else
            {
                if (llGetAgentInfo(pilot) & AGENT_ON_OBJECT)
                {
                    OnBoard = FALSE;                                    // seated but not onboard
                                                                        // can we use this boat?
                                                                        // for each mode see if we're allowed.
                    if (pbsAccess == pbsPermPrivate)                    // if owner access, check owner
                    {
                        if (pilot == owner)
                        {
                            OnBoard = TRUE;
                        }
                    }


                    if (pbsAccess == pbsPermGroup)                      // if group access, check group
                    {
                                
                                key groupKey = llList2Key(llGetObjectDetails(llGetLinkKey(LINK_ROOT), [OBJECT_GROUP]), 0);
                                if (llSameGroup(groupKey))
                                {
                                    llWhisper(0,"Group Access");
                                    OnBoard = TRUE;
                                }
                    }
                          
                    if (pbsAccess == pbsPermPublic)                     // if public access, well, just let em sit
                    {
                                    llWhisper(0,"Public Access");
                                    OnBoard = TRUE;
                    }
                        
                    if (OnBoard)
                    {    
                        llWhisper(0,"you can use this boat");
                     
                        llRequestPermissions(pilot,PERMISSION_TAKE_CONTROLS | PERMISSION_TRIGGER_ANIMATION);
                        pbsInit();
                        pbsPilotPose = pilot;
                        llStopAnimation("sit");
                        llStartAnimation(pbsPose);
                        pbsEngineStart();
                    }
                    else
                    {
                        llWhisper(0,"sorry, you cannot use this boat.");
                        llUnSit(pilot);
                    }                                           // if (OnBoard)
                    
                }                                               // if (llGetAgentInfo(pilot) & AGENT_ON_OBJECT)
                
            }                                                   // if (pilot==NULL_KEY)
        }                                                       // if (change & CHANGED_LINK)
    }               // end changed event
    
    run_time_permissions(integer pbsPerms)
    {
        if (pbsPerms & PERMISSION_TAKE_CONTROLS)
        {
            llTakeControls(CONTROL_RIGHT |
                                  CONTROL_LEFT  |
                                  CONTROL_ROT_RIGHT |
                                  CONTROL_ROT_LEFT |
                                  CONTROL_FWD |
                                  CONTROL_BACK |
                                  CONTROL_DOWN |
                                  CONTROL_UP,
                                  TRUE, FALSE);
            permSet = TRUE;
        }
    }
    
    control(key id, integer held, integer change)
    {
        
            if ((change & held & CONTROL_ROT_LEFT) || (held & CONTROL_ROT_LEFT)) // left turn
            {
                if (pbsHold)
                {
                    llWhisper(0,"Course hold disengaged");
                    pbsHold = FALSE;
                }

                llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <-speedTurn*1.2,0.0,(1.0/speedTurn)/10.0>);
            }

        
            if ((change & held & CONTROL_ROT_RIGHT) || (held & CONTROL_ROT_RIGHT)) // right turn
            {
                if (pbsHold)
                {
                    llWhisper(0,"Course hold disengaged");
                    pbsHold = FALSE;
                }


                llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <speedTurn*1.2,0.0,-(1.0/speedTurn)/10.0>);
            }
                    
            if  (change & held & CONTROL_FWD) // throttle up 
            {
                if (throttle < throttleMax)
                {
                    throttle = throttle + 1;
                    llWhisper(0,"throttle "+(string)throttle);
                }
            }
        
            if  (change & held & CONTROL_BACK) // throttle down 
            {
                if (throttle >  -throttleMax)
                {
                    throttle = throttle - 1;
                    llWhisper(0,"throttle "+(string)throttle);
                }
            }
            
            if  (change & held & CONTROL_DOWN) // anchor or moor
            {

                if (pbsMode == pbsModeRunning)
                {
                    pbsMode = pbsModeDocked;
                    llWhisper(0,"Docking, no drift");
                }
                else
                {
                    if (pbsMode == pbsModeDocked)
                    {
                        pbsMode = pbsModeStopped;
                        llSetStatus(STATUS_PHYSICS,FALSE);
                        pbsEngineStop();
                        llStopSound();
                        llWhisper(0,"Stopped");
                    }
                    else
                    {
                        if (pbsMode == pbsModeStopped)
                        {
                            llSetStatus(STATUS_PHYSICS,FALSE);
                            pbsEngineStop();
                            llStopSound();
                            llWhisper(0,"Anchored");
                            pbsMode = pbsModeAnchored;
                        }
                        else
                        {
                            if (pbsMode == pbsModeAnchored)
                            {
                                pbsEngineStop();
                                llStopSound();
                                llWhisper(0,"Moored");
                                pbsMode = pbsModeMoored;
                            }
                            else
                            {
                                pbsInit();
                                pbsEngineStart();
                                
                            }
                        }
                    }
                }
                            
            }            

            if  (change & held & CONTROL_UP) // engage course hold
            {
                if (pbsMode == pbsModeDocked)
                {
                    pbsMode = pbsModeRunning;
                    llWhisper(0,"drift enabled");
                }
                else
                {
                    llWhisper(0,"Heading hold enabled, heading "+(string)pbsCompass);
                    pbsCourseHoldValue = pbsCompass;
                    pbsHold = TRUE;
                }
            }

        
            if ((change & held & CONTROL_RIGHT)) // Menu Select (or if in mouselook, turn right as normal)
            {
                
                if (mouseLook & AGENT_MOUSELOOK)
                {
                    if (pbsHold)
                    {
                        llWhisper(0,"Course hold disengaged");
                        pbsHold = FALSE;
                    }

                    llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <speedTurn*1.2,0.0,-(1.0/speedTurn)/10.0>);
                }
                else
                {
                    pbsMenuMain();
                }
            }
            
            if ((change & held & CONTROL_LEFT)) // Menu Select (or if in mouselook, turn right as normal)
            {
                
                if (mouseLook & AGENT_MOUSELOOK)
                {
                    if (pbsHold)
                    {
                        llWhisper(0,"Course hold disengaged");
                        pbsHold = FALSE;
                    }

                    llSetVehicleVectorParam(VEHICLE_ANGULAR_MOTOR_DIRECTION, <-speedTurn*1.2,0.0,(1.0/speedTurn)/10.0>);
                }
                else
                {
                    pbsGetWXR();
                }
            }      
    }

// Timer event, this is where the action happens.
    
    timer()
    {
        currEuler = llRot2Euler(llGetRot());            // get heading
        zRotAngle = currEuler.z;
        pbsCompass = zRotAngle*RAD_TO_DEG;              // that's the compass, we need it for heading hold and drift
        pbsCompass = (450.0 - pbsCompass)%360.0;
        mouseLook = llGetAgentInfo(pilot);              // maybe tell is if we're in mouselook


        pbsDrift();                                     // maybe drift the boat
        if (pbsHold)                                    // maybe correct the course (well, heading really)
        {
            pbsCourse();
        }
        pbsEngine();                                    // make the boat move
        if (timerDiv > 5)
        {
            timerDiv = 0;
            if (pbsUseTransponder)
            {
                pbsTransponder();
            }
        }
        timerDiv++;
        
    }

    listen(integer channel, string name, key id, string msg)
    {
        if (channel==-54001)
        {
              
                list lines=llParseString2List(msg,["\n"],[]);
                
                if (llList2String(lines,0)=="wind")             // look for a wind message
                {
                    list parse=llParseString2List(llList2String(lines,1),["=",";","/"],[]);
                    string var=llList2String(parse,0);
                    string val=llList2String(parse,1);
                    if (var=="wvel")                            // take wind velocity as a vector
                    {
                        wind=(vector)val;
                        windSpeed = llVecMag(wind);              // m/s for the calculations
                        windSpeed = windSpeed*1.94;             // change wind speed to knots
                        windDir = ((llAtan2(wind.x,wind.y)* RAD_TO_DEG)+180.0); // true direction
                        windDir = (450.0 - windDir)%360.0;              // corrected
                        windDir = windDir*DEG_TO_RAD;
                    }
                }
            
        }
        
        if (channel == pbsMenuChan)
        {
            llWhisper(0,msg);
            if (msg== "Settings")
            {
                pbsDisplaySettings();
            }
            
            if (msg == "Perms")
            {
                pbsMenuPerm();
            }
            
            if (msg == "Wind")
            {
                pbsMenuWind();
            }
            
            if (msg == "Engine")
            {
                pbsMenuEngine();
            }
            
            if (msg == "Save")
            {
                llWhisper(0,"Saving Settings");
                pbsSaveDefaultState();
            }
            
            if (msg == "Advanced")
            {
                pbsSetEngineAdvanced = TRUE;
                llWhisper(0,"Advanced engine settings enabled");
            }
            if (msg == "Easy")
            {
                pbsSetEngineAdvanced = FALSE;
                llWhisper(0,"Advanced engine settings disabled");
            }
            
            if (msg == "Owner")
            {
                pbsAccess = pbsPermPrivate;
                llWhisper(0,"Private access is set");
            }
            if (msg == "Group")
            {
                pbsAccess = pbsPermGroup;
                llWhisper(0,"Group access is set");
            }
            if (msg == "Public")
            {
                pbsAccess = pbsPermPublic;
                llWhisper(0,"Public access is set");
            }
            if (msg == "System")
            {
                pbsSetWindType = pbsUseSystemWind;
                llWhisper(0,"System wind in use");
            }
            if (msg == "Setter")
            {
                pbsSetWindType = pbsUseSetterWind;
                llWhisper(0,"Setter wind in use");
            }
            
        }
    }                
}       