// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// Licensed GPL, look at the license file.
// The API to this system is open, feel free to write your own accessories.
//
// Accessory to the PBS. This implements an engine's sounds including idle.
// Inventory of sounds needs to be nearby (root prim or this prim at least)
// It will receive messages from the PBS main script. 

// this version is 0.1 let's get it running
//

// Global Constants (PBS-Constants.h)
//
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeMoored = 1;                      // moored, nobody onboard, drifts with wind
integer pbsModeAnchored = 2;                    // anchored, someone onboard, drifts with wind
integer pbsModeRunning = 3;                     // running, at least a pilot is on board
integer pbsModeAdrift = 4;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 5;                     // Only owner can pilot 
integer pbsPermGroup = 6;                      // Group members can pilot
integer pbsPermPublic = 7;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

// variables for this script

string currentSound;
string engineSoundBase = "pbsEngine";
string engineSound;
float enginePitch;
integer throtidx;
integer throttleMax = 20;                       // this has to match up with the throttle max on pbs.
float throttleScale;
integer throt;

playEngine(integer throttle)
{
    throttleScale=.5;
    if (throttle==0)
    {
        enginePitch = 3;
    }
    else
    {
        enginePitch=throttle*throttleScale;

        if (enginePitch < 1)
        {
            enginePitch=1;
        }
    }
    throtidx = (integer)enginePitch; 
    engineSound = engineSoundBase+(string)throtidx;
    llLoopSound(engineSound,1.0);
}

default
{
    state_entry()
    {
            llOwnerSay("Motor Present");
            throttleScale=10 / throttleMax;
    }
    
    link_message(integer Sender, integer Number, string message, key key_id)
    {
        if (Number==pbsMsgStop)
        {
              llStopSound();
        }
        
        if (Number==pbsMsgStart)
        {
            llPlaySound("pbsEngineStart",1.0);
        }
        
        if (Number==pbsMsgThrottle)
        {
            throt=(integer)message;
            playEngine(throt);
        }
    }
}