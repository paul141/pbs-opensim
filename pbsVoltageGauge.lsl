// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// GPL License, see the LICENSE file.
// The API to this system is open, feel free to write your own accessories.
//
// Accessory to the PBS. This implements a Battery Voltage gauge.
// 
// It will receive messages from the PBS main script. 

// this version is 1.0 
//
// Global Constants (PBS-Constants.h)
//
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeRunning = 1;                     // running, at least a pilot is on board
integer pbsModeStopped = 2;                     // stop engine
integer pbsModeAnchored = 3;                    // anchored, someone onboard, drifts with wind
integer pbsModeMoored = 4;                      // moored, nobody onboard, drifts with wind
integer pbsModeAdrift = 5;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 2000;                     // Only owner can pilot 
integer pbsPermGroup = 2001;                      // Group members can pilot
integer pbsPermPublic = 2002;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current
integer pbsMsgDirection = 29;               // transmission direction

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

integer pbsSetEngineAdvanced;
integer pbsSetWindType;
integer pbsPerms;                          // Who can drive this boat?
integer pbsLastMenu;                        // I think we save the menu

integer pbsMSelClose = 1000;
integer pbsMSelMain = 1001;
integer pbsMSelEngine = 1002;
integer pbsMSelWind = 1003;
integer pbsMSelPerm = 1004;

integer pbsUseSystemWind = 2020;
integer pbsUseSetterWind = 2021;


// variables for this script

float Volts;
float angval;
float VoltMax = 16.0;


moveNeedle(float Angle)
{
    llSetLinkPrimitiveParamsFast(LINK_THIS,[PRIM_TEXTURE,ALL_SIDES,"NeedleIndicator",<1,1,0>,<0,0,0>,Angle]);
}

default
{
    on_rez(integer dumdum)
    {
        moveNeedle(0.0);
    }
    state_entry()
    {
        moveNeedle(0.0);
    }

   link_message(integer sender, integer num, string msg, key id)
    {
        if (num == pbsMsgStop)
        {
            Volts = 7.0;                  // stop message shuts off the gauge
        }
            
        if (num == pbsMsgVolts)
        {

            Volts = (float)msg;
            
            Volts = Volts/VoltMax;
 

        }
            angval = llFabs(TWO_PI*(Volts*0.27));        
            moveNeedle(-angval);
    }
}    