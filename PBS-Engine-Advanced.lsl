// Power Boat System
// (c) Paul Harriman (dba Edison Rex) 2018
// GPL license applies. 
// The API to this system is open, feel free to write your own accessories.
//
// Accessory to the PBS. This implements an engine's sounds including idle.
// Inventory of sounds needs to be nearby (root prim or this prim at least)
// It will receive messages from the PBS main script. 

// this version is 1.2
// 1.0 simply did engine steps with idle.
// 1.1 preloads, adds transitions, queuing
//

// Global Constants (PBS-Constants.h)
//
// mode constants (operating mode)
integer pbsModeDocked = 0;                      // docked, no drift
integer pbsModeMoored = 1;                      // moored, nobody onboard, drifts with wind
integer pbsModeAnchored = 2;                    // anchored, someone onboard, drifts with wind
integer pbsModeRunning = 3;                     // running, at least a pilot is on board
integer pbsModeAdrift = 4;                      // adrift, people or not, this is not good

// permission constants
integer pbsPermPrivate = 5;                     // Only owner can pilot 
integer pbsPermGroup = 6;                      // Group members can pilot
integer pbsPermPublic = 7;                      // Anybody can pilot

// pbs messages, these are sent to linked prims, they can also go to HUDs or other listeners

integer pbsMsgStop = 10;                    // stop and reset

integer pbsMsgStart = 20;                   // engine started
integer pbsMsgThrottle = 21;                // throttle message, string has the value
integer pbsMsgFuel = 22;                    // fuel message, string has the value
integer pbsMsgOil = 23;                     // oil temp, string has the value
integer pbsMsgH2oTemp = 24;             // water temp, string has the value
integer pbsMsgWarn = 25;                    // warning, string will have a list of conditions
integer pbsMsgTach = 26;                    // tach, string has the value
integer pbsMsgVolts = 27;                   // battery, string has the voltage
integer pbsMsgAmps = 28;                    // ammeter, string has the current

integer pbsMsgWake = 30;                    // messages to wake generators
integer pbsMsgSmoke = 31;                   // messages to smoke generators
integer pbsMsgWindVane = 32;                // messages to windvanes

// variables for this script

string currentSound;
string engineSoundBase = "Outboard";
string engineSound;
float enginePitch;
integer throtidx;
integer throtLast;
integer throttleMax = 10;                       // this has to match up with the throttle max on pbs.
float throttleScale=1.0;
integer throt;
integer numSounds;
integer sndidx;

playEngine(integer throttle)
{
    if (throttle==0)
    {
        enginePitch = 0;
    }
    else
    {
        enginePitch=throttle*throttleScale;

        if (enginePitch < 0.0)
        {
            enginePitch=llFabs(enginePitch);
        }
    }
    throtidx = (integer)enginePitch; 
    // play the transition first
    if (throtidx > throtLast)
    {
        engineSound = engineSoundBase+"TU"+(string)throtidx;             // play transition up sound
        llPlaySound(engineSound,1.0);
    }
    else
    {
        if (throtidx != throtLast)
        {
            engineSound = engineSoundBase+"TD"+(string)throtidx;             // play transition down sound
            llPlaySound(engineSound,1.0);
        }
    }
    // now loop the engine pitch
     engineSound = engineSoundBase + "L" + (string)throtidx;              // engine loop sound
     llLoopSound(engineSound,1.0);

}

default
{
    state_entry()
    {
            llOwnerSay("Motor Present");
            throttleScale=10 / throttleMax;
            llSetSoundQueueing(TRUE);               // sound queueing is a thing I hope
            numSounds = llGetInventoryNumber(INVENTORY_SOUND);  // get the sounds
            for (sndidx=0; sndidx<numSounds; sndidx++)
            {
                llPreloadSound(llGetInventoryName(INVENTORY_SOUND,sndidx));
            }
            llOwnerSay("finished preloading.");
    }
    
    link_message(integer Sender, integer Number, string message, key key_id)
    {
        if (Number==pbsMsgStop)
        {
              llStopSound();
        }
        
        if (Number==pbsMsgStart)
        {
            llPlaySound("pbsEngineStart",1.0);
        }
        
        if (Number==pbsMsgThrottle)
        {
            throtLast = throt;
            throt=(integer)message;
            playEngine(throt);
        }
    }
}